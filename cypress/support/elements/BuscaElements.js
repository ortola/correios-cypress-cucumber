class BuscaElements {
    campoInformarCepOuEndereco = () => {
        return '#endereco'
    }

    selecionarTipoCep = () => {
        return 'div .controle select'
    }

    clicarBotaoBuscar = () => {
        return '#btn_pesquisar'
    }

    retornoBuscaSucesso = () => {
        return '#navegacao-total'
    }

    logradouroRetornoBusca = () => {
        return '#resultado-DNEC > tbody > tr > td:nth-child(1)'
    }

    bairroRetornoBusca = () => {
        return '#resultado-DNEC > tbody > tr > td:nth-child(2)'
    }

    localidadeRetornoBusca = () => {
        return '#resultado-DNEC > tbody > tr > td:nth-child(3)'
    }

    cepRetornoBusca = () => {
        return '#resultado-DNEC > tbody > tr > td:nth-child(4)'
    }

    msgDadosNaoEncontrados = () => {
        return '#mensagem-resultado'
    }

    msgAlert = () => {
        return '.msg'
    }
}

export default BuscaElements;