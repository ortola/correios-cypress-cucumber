// <reference types="Cypress" />

import BuscaElements from '../elements/BuscaElements'
const buscaElements = new BuscaElements
const url = Cypress.config("baseUrl")

class BuscaPage {
    
    // Acessa o site que será testado
    acessarSite() {
        cy.visit(url)
    }

    informarCepOuEndereco(endereco){
        cy.get(buscaElements.campoInformarCepOuEndereco()).type(endereco)
    }

    selecionarTipoCep(tipoCep){
        cy.get(buscaElements.selecionarTipoCep()).select(tipoCep)
    }

    clicarBotaoBuscar(){
        cy.get(buscaElements.clicarBotaoBuscar()).click();
    }

    retornoBuscaSucesso(){
       cy.get(buscaElements.retornoBuscaSucesso()).should("exist");
     }

    retornoLogradouro(retornoLogradouro){
        cy.get(buscaElements.logradouroRetornoBusca()).should("contain", retornoLogradouro);
    }

    retornoBairro(retornoBairro){
        cy.get(buscaElements.bairroRetornoBusca()).should("contain", retornoBairro);
    }

    retornoLocalidade(retornoLocalidade){
        cy.get(buscaElements.localidadeRetornoBusca()).should("contain", retornoLocalidade);
    }

    retornoCep(retornoCep){
        cy.get(buscaElements.cepRetornoBusca()).should("contain", retornoCep);
    }

    retornoMsgDadosNaoEncontrados(msg){
        cy.get(buscaElements.msgDadosNaoEncontrados()).should("contain", msg)
    }

    retornoMsgAlert(msgAlert){
        cy.get(buscaElements.msgAlert()).should("contain", msgAlert)
    }
}

export default BuscaPage;