/* global Given, Then, When */

import BuscaPage from '../pageobjects/BuscaPage'
const buscaPage = new BuscaPage

Given("acesso o site dos correios", () => {
    buscaPage.acessarSite();
})

And("informo um cep ou endereco {string}", (endereco) => {
    buscaPage.informarCepOuEndereco(endereco);
})

And("seleciono um tipo de cep {string}", (tipoCep) => {
    buscaPage.selecionarTipoCep(tipoCep);
})

When("clicar no botão buscar", () => {
    buscaPage.clicarBotaoBuscar();
})

Then("sistema retorna o resultado", () => {
        buscaPage.retornoBuscaSucesso();
    })

And ("deve retornar no campo logradouro-nome o valor {string}", (logradouroRetorno) => {
    buscaPage.retornoLogradouro(logradouroRetorno);
})

And ("deve retornar no campo bairro-distrito o valor {string}", (bairroRetorno) => {
    buscaPage.retornoBairro(bairroRetorno);
})

And ("deve retornar no campo localidade-uf o valor {string}", (localidadeRetorno) => {
    buscaPage.retornoLocalidade(localidadeRetorno);
})

And ("deve retornar no campo cep o valor {string}", (cepRetorno) => {
    buscaPage.retornoCep(cepRetorno);
})

And("deve retornar a seguinte mensagem {string}", (msg) => {
    buscaPage.retornoMsgDadosNaoEncontrados(msg);       
})

And("deve retornar a mensagem de impedimento {string}", (msgAlert) => {
    buscaPage.retornoMsgAlert(msgAlert)
})