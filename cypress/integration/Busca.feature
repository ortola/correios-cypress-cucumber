Feature: Cusca por CEP ou Endereço no site dos correios

Scenario Outline: '<cenario>'
    Given acesso o site dos correios
    And informo um cep ou endereco '<cep>'
    And seleciono um tipo de cep '<tipoCep>'
    When clicar no botão buscar
    Then sistema retorna o resultado
    And deve retornar no campo logradouro-nome o valor '<logradouroRetorno>'
    And deve retornar no campo bairro-distrito o valor '<bairroRetorno>'
    And deve retornar no campo localidade-uf o valor '<localidadeRetorno>'
    And deve retornar no campo cep o valor '<cepRetorno>'
    Examples:
    |cenario               |cep     |tipoCep              |logradouroRetorno              |bairroRetorno|localidadeRetorno|cepRetorno|
    |01 - Buscar cep válido|21721600|Localidade/Logradouro|Travessa Marechal Marciano     |Realengo     |Rio de Janeiro/RJ|21721-600 |    
    |02 - Buscar cep válido|21875240|Todos                |Rua Barra do Corda             |Bangu        |Rio de Janeiro/RJ|21875-240 |
    |03 - Buscar cep válido|21725580|Todos                |Rua Jussara Ferreira dos Santos|Bangu        |Rio de Janeiro/RJ|21725-580 |

Scenario: 04 - Buscar CEP não existente
 Given acesso o site dos correios
 And informo um cep ou endereco 'ABCDEFGHIJ' 
 And seleciono um tipo de cep 'Localidade/Logradouro'
 When clicar no botão buscar
 Then sistema retorna o resultado
 And deve retornar a seguinte mensagem 'Não há dados a serem exibidos'


 Scenario: 05 - Realizar uma busca não preenchendo o campo CEP (em branco)
 Given acesso o site dos correios
 And informo um cep ou endereco ' ' 
 And seleciono um tipo de cep 'Localidade/Logradouro'
 When clicar no botão buscar
 Then sistema retorna o resultado
 And deve retornar a mensagem de impedimento 'Informe o Endereço com no mínimo 2(dois) caracteres!'




